import scala.io.Source


case class Maze(val in: Int = 0, val out: Int = 0, matrix: Map[Int, List[Int]] = Map())
case class foldAccumulator(nodes: Set[Int] = Set(), position: Int = 0, in: Int = -1, out: Int = -1)

object MazeSolver {
  def extractFile(fileName: String): List[List[Char]] = {
    Source.fromFile(fileName).getLines().foldLeft(List.empty[List[Char]])((acc, c) => acc :+ c.toList)
  }

  def parseContent(list: List[List[Char]]): Maze = {
    val depth = list.size
    val width = list.headOption.map(_.size).fold(0)(l => l)

    val parsed: foldAccumulator = list.flatten.foldLeft(foldAccumulator())((a, c) => {
      val currentPost = a.position + 1
      c match {
        case '*' => a.copy(position = currentPost)
        case 'I' => a.copy(position = currentPost, in = currentPost, nodes = a.nodes + currentPost)
        case 'O' => a.copy(position = currentPost, out = currentPost, nodes = a.nodes + currentPost)
        case ' ' => a.copy(position = currentPost, nodes = a.nodes + currentPost)
        case _ => throw new IllegalArgumentException("only [' ', '*', 'I', 'O'] are valid to define your Maze")
      }
    })
    val nodes = parsed.nodes
    val matrix: Map[Int, List[Int]] = nodes.map { e =>
      (e -> Set(e - 1, e + 1, e - width, e + width)
        .filter(nodes.contains(_))
        .filterNot(a => a < 0 || a > depth * width)
        .filterNot(a => a % width == 0).toList)
    }.toMap
    Maze(parsed.in, parsed.out, matrix)
  }


  type vertex = Int
  type path = (Int, List[vertex])
  type adjacencyMatrix = Map[vertex, List[vertex]]

  //modified dijkstra where all adjacents are weight 1
  def Dijkstra(aMatrix:adjacencyMatrix, fringe: List[path], dest: vertex, visited: Set[vertex]): path = {
    fringe match {
      case (dist, path) :: tail => path match {
        case key :: path_rest =>
          if (key == dest) (dist, path.reverse)
          else {
            val paths = aMatrix(key).flatMap {
              case v => if (!visited.contains(v)) List((dist + 1, v :: path)) else Nil
            }
            Dijkstra(aMatrix,(paths ++ tail), dest, visited + key)
          }
        case _ => throw new RuntimeException("This maze is pzzling the maze solver")
      }
      case Nil => (0, List())
    }
  }
}

object Main {
  def main(args: Array[String]): Unit = {
    println("please let me know which is the full path to your maze file")
    val ln = scala.io.StdIn.readLine()
    val lines = MazeSolver.extractFile(ln)
    val parsed = MazeSolver.parseContent(lines)
    val (weight, list ) = MazeSolver.Dijkstra(parsed.matrix, List((0, List(parsed.in))), parsed.out, Set())
    println(s"${list.map(_-1).mkString("-")}")
  }
}